# https://www.elastic.co/guide/en/elasticsearch/reference/current/query-dsl-fuzzy-query.html

# fuzzy query podmienia okreslona liczbe znakow (param fuzziness) na dowolny
# idealne rozwiazanie na literówki

curl -XGET 'localhost:9200/bank/_search?pretty' -H 'Content-Type: application/json' -d'
{
    "query": {
        "fuzzy" : {
            "firstname" : {
                "value": "kery",
                "boost": 1.0,
                "fuzziness": 1
            }
        }
    }
}
'
# to samo zapytanie z wieksza ilosc fuzziness
curl -XGET 'localhost:9200/bank/_search?pretty' -H 'Content-Type: application/json' -d'
{
    "query": {
        "fuzzy" : {
            "firstname" : {
                "value": "kery",
                "boost": 1.0,
                "fuzziness": 2
            }
        }
    }
}
'