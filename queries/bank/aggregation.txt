# https://www.elastic.co/guide/en/elasticsearch/guide/current/aggregations.html

# agregacja pola age
curl -XGET 'localhost:9200/bank/_search?pretty' -H 'Content-Type: application/json' -d'
{
    "aggs" : {
        "ages" : {
            "terms" : { "field" : "age" }
        }
    }
}
'

# sortowanie otrzymanych agregacji po ilosci
curl -XGET 'localhost:9200/bank/_search?pretty' -H 'Content-Type: application/json' -d'
{
    "aggs" : {
        "ages" : {
            "terms" : {
                "field" : "age",
                "order" : { "_count" : "asc" }
            }
        }
    }
}
'

# sortowanie agregacji po kluczu
curl -XGET 'localhost:9200/bank/_search?pretty' -H 'Content-Type: application/json' -d'
{
    "aggs" : {
        "ages" : {
            "terms" : {
                "field" : "age",
                "order" : { "_key" : "asc" }
            }
        }
    }
}
'

# limit na agregacje do 5
curl -XGET 'localhost:9200/_search?pretty' -H 'Content-Type: application/json' -d'
{
    "aggs" : {
        "ages" : {
            "terms" : {
                "field" : "age",
                "size" : 5
            }
        }
    }
}
'

