# https://www.elastic.co/guide/en/elasticsearch/guide/current/getting-started.html

# info o indeksie, mappingu pól
curl -XGET 'localhost:9200/bank?pretty'
# pobranie informacji o koncie o numerze 1
curl -XGET 'localhost:9200/bank/account/1?pretty'